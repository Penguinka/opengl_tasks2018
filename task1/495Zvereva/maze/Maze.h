#pragma once

#include <vector>
#include <string>
#include <fstream>

struct Maze
{

    size_t _startX, _startY, _sizeX, _sizeY;
    std::vector<std::vector<bool> > _maze;

    // # - занятая клетка
    // * - проходимая клетка
    Maze(const std::string filepath) {
        std::ifstream _mazeIn(filepath.c_str());
        _mazeIn >> _sizeX >> _sizeY;
        _mazeIn >> _startX >> _startY;
        _startX--;
        _startY--;
        char cell;
        _maze = std::vector<std::vector<bool> >(_sizeY, std::vector<bool>(_sizeX, true));
        for (size_t y = 0; y < _sizeY; ++y) {
            for (size_t x = 0; x < _sizeX; ++x) {
                _mazeIn >> cell;
                if (cell == '#') {
                    _maze[y][x] = false;
                }
            }
        }
    }
};
