#pragma once

#include <Camera.hpp>
#include "Maze.h"
#include <memory>

class MazeCameraMover : public CameraMover
{
public:
    MazeCameraMover(std::shared_ptr<Maze> maze);

    void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    void update(GLFWwindow* window, double dt) override;

protected:
    glm::vec3 _pos;
    glm::quat _rot;
    std::shared_ptr<Maze> _maze;
    //Положение курсора мыши на предыдущем кадре
    double _oldXPos = 0.0;
    double _oldYPos = 0.0;
};

