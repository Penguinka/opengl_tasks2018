#include "MazeCamera.h"
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <iostream>

MazeCameraMover::MazeCameraMover(std::shared_ptr<Maze> maze) :
        CameraMover(),
        _maze(maze)
{
    //Поместим камеру в точку старта лабиринта
    float x = float(_maze->_startX) + 0.5f;
    float y = float(_maze->_startY) + 0.5f;
    _pos = glm::vec3(y, x, 0.5f);
    glm::vec3 _direction = glm::vec3(_maze->_sizeY / 2.f, _maze->_sizeX / 2.f, 0.5f);
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, _direction, glm::vec3(0.0f, 0.0f, 1.0f)));
}

void MazeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void MazeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void MazeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void MazeCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 1.0f;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    //Скорость в горизонтальном направлении не зависит от направления взгляда камеры
    rightDir /= sqrt(rightDir.x * rightDir.x + rightDir.y * rightDir.y);
    forwDir /= sqrt(forwDir.x * forwDir.x + forwDir.y * forwDir.y);

    glm::vec3 _deltaPos(0.0f, 0.0f, 0.0f);

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _deltaPos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _deltaPos -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        _deltaPos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        _deltaPos += rightDir * speed * static_cast<float>(dt);
    }

    if (_deltaPos[0] > 0 &&
        (int(_pos[0] + std::max(0.15f, _deltaPos[0])) + 1 > _maze->_sizeY ||
         !_maze->_maze[int(_pos[0] + std::max(0.15f, _deltaPos[0]))][_pos[1]]) ||
        _deltaPos[0] < 0 && ((_pos[0] + std::min(-0.15f, _deltaPos[0])) < 0 ||
                         !_maze->_maze[int(_pos[0] + std::min(-0.15f, _deltaPos[0]))][_pos[1]]))
    {
        _deltaPos[0] = 0;
    }
    if (_deltaPos[1] > 0 &&
        (int(_pos[1] + std::max(0.15f, _deltaPos[1])) + 1 > _maze->_sizeX ||
         !_maze->_maze[_pos[0]][int(_pos[1] + std::max(0.15f, _deltaPos[1]))]) ||
        _deltaPos[1] < 0 && ((_pos[1] + std::min(-0.15f, _deltaPos[1])) < 0 ||
                         !_maze->_maze[_pos[0]][_pos[1] + std::min(-0.15f, _deltaPos[1])]))
    {
        _deltaPos[1] = 0;
    }
    _deltaPos[2] = 0;
    _pos += _deltaPos;
    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
}

