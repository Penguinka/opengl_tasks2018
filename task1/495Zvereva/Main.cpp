#include <Application.hpp>
#include <ShaderProgram.hpp>
#include <Camera.hpp>
#include <Mesh.hpp>
#include <LightInfo.hpp>
#include "maze/Maze.h"
#include "maze/MazeCamera.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <Common.h>
#include <GL/gl.h>

class MazeApplication : public Application
{
public:

    std::shared_ptr<Maze> _maze;
    int _isOrbitCamera = 0;
    int _currentCam = 0; // 0 - inside; 1 - outside

    std::shared_ptr<CameraMover> _altCameraMover;
    CameraInfo _altCamera;

    ShaderProgramPtr _shader;
    MeshPtr _mesh;

    MazeApplication(std::shared_ptr<Maze> maze):
            _maze(maze) {}

    MeshPtr makeMaze(){
        // координаты углов первого треугольника
        // нормали направленные вверх
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        for( int y = 0; y < _maze->_sizeY; ++y ){
            for( int x = 0; x < _maze->_sizeX; ++x){
                // для пола
                if ( _maze->_maze[y][x]) {
                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y + 1.f, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y + 1.f, x, -0.5f));

                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y + 1.f, x, -0.5f));
                    vertices.push_back(glm::vec3(y, x, -0.5f));

                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
                }
                else {
                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y + 1.f, x, -0.5f));
                    vertices.push_back(glm::vec3(y + 1.f, x - 1, -0.5f));

                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y, x, -0.5f));
                    vertices.push_back(glm::vec3(y + 1.f, x, -0.5f));

                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

                }
                //для потолка
                // чтобы потолок был виден, если мы внутри и не виден снаружи
                if (!_maze->_maze[y][x]) {
                    vertices.push_back(glm::vec3(y, x - 1, 0.5f));
                    vertices.push_back(glm::vec3(y + 1.f, x - 1, 0.5f));
                    vertices.push_back(glm::vec3(y + 1.f, x, 0.5f));

                    vertices.push_back(glm::vec3(y, x - 1, 0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, 0.5f));
                    vertices.push_back(glm::vec3(y, x, 0.5f));

                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));

                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));
                }
                else {

                    vertices.push_back(glm::vec3(y, x - 1, 0.5f));
                    vertices.push_back(glm::vec3(y + 1.f, x, 0.5f));
                    vertices.push_back(glm::vec3(y + 1.f, x - 1, 0.5f));

                    vertices.push_back(glm::vec3(y, x - 1, 0.5f));
                    vertices.push_back(glm::vec3(y, x, 0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, 0.5f));

                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));

                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));
                    normals.push_back(glm::vec3(0.0f, 0.0f, -1.0f));
                }

                // стена север для занятой клетки
                if(_maze->_maze[y][x] && y > 0 && !_maze->_maze[y - 1][x]) {

                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y, x, 0.5f));
                    vertices.push_back(glm::vec3(y, x - 1, 0.5f));

                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y, x, -0.5f));
                    vertices.push_back(glm::vec3(y, x, 0.5f));

                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));

                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));

                }
                // стена север для границы лабиринта
                if (y == 0 && !_maze->_maze[y][x]) {
                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y, x - 1, 0.5f));
                    vertices.push_back(glm::vec3(y, x, 0.5f));

                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y, x, 0.5f));
                    vertices.push_back(glm::vec3(y, x, -0.5f));

                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));

                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, 1.f, 0.0f));

                }



                if(_maze->_maze[y][x] && y + 1 < _maze->_sizeY && !_maze->_maze[y + 1][x]) {
                    // стена юг
                    vertices.push_back(glm::vec3(y + 1, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x - 1, 0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, 0.5f));

                    vertices.push_back(glm::vec3(y + 1, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, 0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, -0.5f));

                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));

                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));
                }

                if (y + 1 == _maze->_sizeY && !_maze->_maze[y][x]) {
                    vertices.push_back(glm::vec3(y + 1, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, 0.5f));
                    vertices.push_back(glm::vec3(y + 1, x - 1, 0.5f));

                    vertices.push_back(glm::vec3(y + 1, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, 0.5f));

                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));

                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));
                    normals.push_back(glm::vec3(0.0f, -1.f, 0.0f));
                }

                if(_maze->_maze[y][x] && x + 1 < _maze->_sizeX && !_maze->_maze[y][x + 1]) {
                    // стена запад
                    vertices.push_back(glm::vec3(y, x, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, 0.5f));
                    vertices.push_back(glm::vec3(y, x, 0.5f));

                    vertices.push_back(glm::vec3(y, x, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, 0.5f));

                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));

                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));
                }
                if (_maze->_sizeX == x + 1 && !_maze->_maze[y][x]) {

                    vertices.push_back(glm::vec3(y, x, -0.5f));
                    vertices.push_back(glm::vec3(y, x, 0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, 0.5f));

                    vertices.push_back(glm::vec3(y, x, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, 0.5f));
                    vertices.push_back(glm::vec3(y + 1, x, -0.5f));

                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));

                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(1.0f, 0.f, 0.0f));
                }

                if(_maze->_maze[y][x] && x > 0 && !_maze->_maze[y][x - 1]){
                    // стена восток
                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y, x - 1, 0.5f));
                    vertices.push_back(glm::vec3(y + 1, x - 1, 0.5f));

                    vertices.push_back(glm::vec3(y + 1, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x - 1, 0.5f));

                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));

                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));
                }
                if (x == 0 && !_maze->_maze[y][x]) {
                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x - 1, 0.5f));
                    vertices.push_back(glm::vec3(y, x - 1, 0.5f));

                    vertices.push_back(glm::vec3(y + 1, x - 1, -0.5f));
                    vertices.push_back(glm::vec3(y + 1, x - 1, 0.5f));
                    vertices.push_back(glm::vec3(y, x - 1, -0.5f));

                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));

                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));
                    normals.push_back(glm::vec3(-1.0f, 0.f, 0.0f));
                }

            }
        }
        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());
        return mesh;
    }

    void makeScene() override

    {
        Application::makeScene();

        if (!_isOrbitCamera) {
            _cameraMover = std::make_shared<MazeCameraMover>(_maze);
            _altCameraMover = std::make_shared<OrbitCameraMover>();
        } else {
            _cameraMover = std::make_shared<OrbitCameraMover>();
            _altCameraMover = std::make_shared<MazeCameraMover>(_maze);
        }

        _mesh = makeMaze();
        _mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.5f)));

        _shader = std::make_shared<ShaderProgram>("495ZverevaData/shader.vert", "495ZverevaData/shader.frag");
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Camera"))
            {
                ImGui::RadioButton("inside", &_currentCam, 0);
                ImGui::RadioButton("outside", &_currentCam, 1);
            }
        }
        // переключаем камеру по radio button
        if (_currentCam != _isOrbitCamera){
            _isOrbitCamera = _currentCam;
            std::swap(_camera, _altCamera);
            std::swap(_cameraMover, _altCameraMover);
            _cameraMover->setPosFrom(_altCameraMover);
        }
        ImGui::End();

    }
    void draw() override
    {
        Application::draw();

        //Получаем размеры экрана (окна)
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        //Устанавливаем порт вывода на весь экран (окно)
        glViewport(0, 0, width, height);

        //Отсекаем нелицевые грани
        glEnable(GL_CULL_FACE);

        //Очищаем порт вывода (буфер цвета и буфер глубины)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменные: время и матрицы
        _shader->setFloatUniform("time", static_cast<float>(glfwGetTime())); //передаем время в шейдер

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _mesh->modelMatrix());
        _mesh->draw();

        glUseProgram(0);
    }
};

int main()
{
    std::string filepath = "495ZverevaData/maze.txt";
    Maze maze(filepath);
    MazeApplication app(std::make_shared<Maze>(maze));
    app.start();

    return 0;
}