/*
Вычисляет цвет и координаты вершин прямо в шейдере на основе значения атрибута t
*/

#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

out vec4 color;

void main()
{
    color.rgb = normal.xyz * 0.5 + 0.5;
	color.a = 1.0;
    vec4 pos = vec4(position, 1.0);
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * pos;
}
